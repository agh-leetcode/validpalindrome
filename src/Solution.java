public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String input = "A man, a plan, a canal: Panama";
        String input2 = "race a car";
        System.out.println(isPalindrome(input));
    }

    private boolean isPalindrome(String s) {
        if (s.isEmpty())
            return true;
        String ignorePunctuation = s.replaceAll("[\\W]", "");
        String reversedInput = new StringBuilder(ignorePunctuation).reverse().toString();
        for (int i = 0; i < reversedInput.length() - 1; ) {
            if (Character.toLowerCase(reversedInput.charAt(i)) == Character.toLowerCase(ignorePunctuation.charAt(i))) {
                i++;
            } else {
                return false;
            }
        }
        return true;
    }

    //better solution
    private boolean isPalindrome2(String s) {
        if (s.isEmpty()) {
            return true;
        }
        int start = 0;
        int end = s.length() - 1;
        char charStart, charEnd;
        while(start <= end) {
            charStart = s.charAt(start);
            charEnd = s.charAt(end);
            if (!Character.isLetterOrDigit(charStart)) {
                start++;
            } else if(!Character.isLetterOrDigit(charEnd)) {
                end--;
            } else {
                if (Character.toLowerCase(charStart) != Character.toLowerCase(charEnd)) {
                    return false;
                }
                start++;
                end--;
            }
        }

        return true;
    }
}
